libktoblzcheck (1.53-2) unstable; urgency=medium

  * Bump debhelper from 12 to 13.
  * debian/clean: Delete src/python/__pycache__ too.
  * Package is compliant to Debian Policy 4.5.1 (no changes needed).

 -- Micha Lenk <micha@debian.org>  Tue, 23 Feb 2021 21:33:24 +0100

libktoblzcheck (1.53-1) unstable; urgency=medium

  * New upstream version 1.53.
  * debian/rules: Re-enable running the test-python unit tests.
  * Package is compliant to Debian Policy 4.5.0 (no changes needed).
  * libktoblzcheck1-dev: Ship cmake modules too.

 -- Micha Lenk <micha@debian.org>  Sat, 25 Apr 2020 21:37:25 +0200

libktoblzcheck (1.52-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Use secure URI in debian/watch.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Archive, Name, Repository.

  [ Micha Lenk ]
  * New upstream version 1.52
  * Drop patch 0001_drop_python2_detection.patch (fixed differently upstream).
  * Drop patch fix_missing_BANKDATA_PATH.patch (the issue got fixed differently
    upstream).
  * Add build dependency on python-all-dev and dh-python to fix build failures.
  * debian/rules: Skip running the broken test-python unit tests. This
    currently prevents packaging the python modules again.

  [ Benjamin Eikel ]
  * The new upstream version now installs the ibanchk executable for checking
    international bank account numbers (IBANs) on the command line (the issue
    of this missing executable was initially reported via Debian but got
    integrated and released by upstream almost immediately).

 -- Micha Lenk <micha@debian.org>  Fri, 10 Apr 2020 23:40:13 +0200

libktoblzcheck (1.50-2) unstable; urgency=medium

  * debian/salsa-ci.yml: Disable build path variations in reprotest.
  * Drop binary package python-ktoblzcheck (Closes: #936886).
  * Add patch to remove the broken Python 2 detection from the upstream build
    system to prevent the package failing to build.

 -- Micha Lenk <micha@debian.org>  Sun, 08 Dec 2019 11:24:26 +0100

libktoblzcheck (1.50-1) unstable; urgency=medium

  * New upstream version 1.50.
  * debian/changelog: Fix spelling error in previous entry (Thanks, Lintian).
  * debian/compat: Bump debhelper compat level (10 -> 11).
  * debian/control:
    - Add build dependency on cmake.
    - Package is compliant to Debian Policy 4.4.1 (no changes needed).
  * debian/rules:
    - Start from scratch because of switch to cmake.
  * debian/docs: Consolidate *.docs files from all packages.
  * debian/source/options: Ignore generated ktoblzcheck.pyc in diff.
  * debian/salsa-ci.yml:
    - Switch to current Salsa CI Team pipeline.
    - Enable diffoscope in reprotest.
  * Add patch fix_missing_BANKDATA_PATH.patch so that the ktoblzcheck binary
    continues to work without explicitly specified bank data file.
  * debian/tests: Update test fixture data used by autopkgtest.

 -- Micha Lenk <micha@debian.org>  Sun, 06 Oct 2019 22:17:58 +0200

libktoblzcheck (1.49-5) unstable; urgency=medium

  * Make package build reproducible in case it is building on a merged /usr
    filesystem (Closes: #915309).
    Thanks to Andreas Henriksson <andreas@fatal.se> for providing the patch.
  * Switch Git repository to salsa.debian.org.
  * Integrate with Salsa CI
  * debian/control:
    - Drop unnecessary testsuite: autopkgtest field (thanks to Lintian).
    - Drop obsolete X-Python-Version field (thanks to Lintian).
    - Package is compliant to Debian Policy 4.3.0 (no changes needed).
    - Mark package libktoblzcheck1v5 as Multi-Arch: same.
    - Drop build-dependency on dh-autoreconf as we are using debhelper compat
      level 10 anyways (thanks to Lintian).
  * debian/changelog: Remove empty line from the end of the file.
    Thanks to Lintian (file-contains-trailing-whitespace).
  * Drop obsolete debian/pycompat file.
    Thanks to Lintian (debian-pycompat-is-obsolete)
  * Bump debhelper compat level (9 -> 10)
  * debian/copyright: Update LGPL 2 to 2.1, matching upstream's COPYING file
  * debian/rules: Add "bindnow" linker flag via DEB_BUILD_MAINT_OPTIONS.
    Thanks to Lintian (hardening-no-bindnow)

 -- Micha Lenk <micha@debian.org>  Sun, 06 Jan 2019 16:35:39 +0100

libktoblzcheck (1.49-4) unstable; urgency=medium

  * debian/tests:
    - Fix illegal-runtime-test-name (flagged by Lintian) for
      test_ktoblzcheck.py by renaming it to test-ktoblzcheck.py.
    - Fix autopkgtest by allowing python-ktoblzcheck.py to print to STDERR.
  * debian/control: Mark package libktoblzcheck1-dev as Multi-Arch: same
    following an advice on the package tracker page.

 -- Micha Lenk <micha@debian.org>  Wed, 28 Jun 2017 23:13:06 +0200

libktoblzcheck (1.49-3) unstable; urgency=medium

  * debian/tests: Trying to fix execution of test_ktoblzcheck.py.

 -- Micha Lenk <micha@debian.org>  Mon, 26 Jun 2017 22:04:49 +0200

libktoblzcheck (1.49-2) unstable; urgency=medium

  * Package is compliant to Debian Policy 4.0.0 (no changes needed).
  * Drop explicit libktoblzcheck-dbg package and switch to automatically
    generated dbgsym packages. This requires debhelper 9.20160114 or newer to
    build (bumped build-dependency accordingly).

 -- Micha Lenk <micha@debian.org>  Sat, 24 Jun 2017 11:23:11 +0200

libktoblzcheck (1.49-1) experimental; urgency=medium

  * New upstream version
  * Package is compliant to Debian Policy 3.9.8 (no changes needed).
  * Bump debhelper compat level (7 -> 9).
    - debian/rules: dh_auto_configure already exports all environment
      variables listed by dpkg-buildflags. This removes the need to explicitly
      call dpkg-buildflags to provide the build flags to ./configure.
    - debian/*.install: dh_auto_install now automatically installs binaries to
      multi-arch locations. The *.install files were changed to reflect that.
  * debian/tests: Add minimal auto-pkg-test using the Python module.

 -- Micha Lenk <micha@debian.org>  Thu, 09 Mar 2017 13:31:30 +0100

libktoblzcheck (1.48-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename library packages for g++ ABI transition,
    patch from Ubuntu (see #791134)
  * Remove override of dh_makeshlibs, no longer required

 -- Jonathan Wiltshire <jmw@debian.org>  Sat, 08 Aug 2015 13:30:21 +0100

libktoblzcheck (1.48-2) unstable; urgency=medium

  * Add a minimalistic autopkgtest.

 -- Micha Lenk <micha@debian.org>  Sun, 03 May 2015 21:45:39 +0200

libktoblzcheck (1.48-1) experimental; urgency=medium

  * New upstream version

 -- Micha Lenk <micha@debian.org>  Mon, 01 Dec 2014 21:17:27 +0100

libktoblzcheck (1.47-1) unstable; urgency=medium

  * New upstream release
  * Dropped patch 01_fix_build_failure_using_gcc-4.9.patch, included upstream.
  * Package is compliant to Debian Policy 3.9.6 (no changes needed).
  * Switch to package management in Git repository. For that purpose, updating
    URLs in Vcs-Browser: and Vcs-Svn: again.

 -- Micha Lenk <micha@debian.org>  Sat, 18 Oct 2014 22:33:26 +0200

libktoblzcheck (1.46-2) unstable; urgency=medium

  * Added patch 01_fix_build_failure_using_gcc-4.9.patch to fix build
    failures when compiling with GCC 4.9 or newer.

 -- Micha Lenk <micha@debian.org>  Mon, 18 Aug 2014 23:13:50 +0200

libktoblzcheck (1.46-1) unstable; urgency=medium

  * New upstream release
  * debian/control: Update URLs in Vcs-Browser: and Vcs-Svn:

 -- Micha Lenk <micha@debian.org>  Fri, 15 Aug 2014 15:44:00 +0200

libktoblzcheck (1.45-1) unstable; urgency=low

  * New upstream release
  * Use dh-autoreconf instead of autotools-dev to fix FTBFS on ppc64el
    (closes: #734536).

 -- Micha Lenk <micha@debian.org>  Tue, 11 Mar 2014 19:49:39 +0100

libktoblzcheck (1.44-1) unstable; urgency=low

  * New upstream release.
  * Update config.{sub,guess} during build using autotools-dev. This should fix
    the build on the arm64 architecture (closes: #727411).
  * Package is compliant to Debian Policy 3.9.5 (no changes needed).

 -- Micha Lenk <micha@debian.org>  Sun, 05 Jan 2014 18:31:50 +0100

libktoblzcheck (1.43-1) unstable; urgency=low

  * New upstream release

 -- Micha Lenk <micha@debian.org>  Wed, 14 Aug 2013 20:17:38 +0200

libktoblzcheck (1.42-1) unstable; urgency=low

  * New upstream release
  * Package is compliant to Debian Policy 3.9.4 (no changes needed).

 -- Micha Lenk <micha@debian.org>  Fri, 07 Jun 2013 15:15:13 +0200

libktoblzcheck (1.41-1) unstable; urgency=low

  * New upstream release

 -- Micha Lenk <micha@debian.org>  Wed, 08 May 2013 13:32:13 +0200

libktoblzcheck (1.40-1) experimental; urgency=low

  * New upstream release
    - fixes online_update.pl script (closes: #686536).
  * Inject build flags from dpkg-buildflags, enabling support for the Security
    Hardening Build Flag release goal.

 -- Micha Lenk <micha@debian.org>  Mon, 12 Nov 2012 20:52:15 +0100

libktoblzcheck (1.39-1) unstable; urgency=low

  * New upstream release

 -- Micha Lenk <micha@debian.org>  Mon, 21 May 2012 22:33:02 +0200

libktoblzcheck (1.38-1) unstable; urgency=low

  * New upstream release
  * Package is compliant to Debian Policy 3.9.3.0 (no changes needed).

 -- Micha Lenk <micha@debian.org>  Mon, 23 Apr 2012 21:45:45 +0200

libktoblzcheck (1.37-1) unstable; urgency=low

  * New upstream release

 -- Micha Lenk <micha@debian.org>  Mon, 12 Dec 2011 19:42:32 +0100

libktoblzcheck (1.36-1) unstable; urgency=low

  * New upstream release

 -- Micha Lenk <micha@debian.org>  Sat, 22 Oct 2011 21:35:49 +0200

libktoblzcheck (1.35-1) unstable; urgency=low

  * New upstream release

 -- Micha Lenk <micha@debian.org>  Fri, 16 Sep 2011 20:05:56 +0200

libktoblzcheck (1.34-1) unstable; urgency=low

  * New upstream release

 -- Micha Lenk <micha@debian.org>  Fri, 26 Aug 2011 19:56:10 +0200

libktoblzcheck (1.33-3) unstable; urgency=low

  * Fix missing Architecture: field for package libktoblzcheck-dbg.
  * Fixed lintian warning about missing ${misc:Depends}.

 -- Micha Lenk <micha@debian.org>  Tue, 16 Aug 2011 09:59:52 +0200

libktoblzcheck (1.33-2) unstable; urgency=low

  * Add package libktoblzcheck-dbg containing the debug symbols.

 -- Micha Lenk <micha@debian.org>  Tue, 16 Aug 2011 09:34:19 +0200

libktoblzcheck (1.33-1) unstable; urgency=low

  * New upstream release that fixes the soname mess.

 -- Micha Lenk <micha@debian.org>  Tue, 07 Jun 2011 23:01:07 +0200

libktoblzcheck (1.32.really.is.1.31-2) unstable; urgency=low

  * Re-upload of upstream version 1.31 to fix weird (backwards) soname bump
    (closes: #629362).
  * Remove lintian override again, lintian was right. Shame on me...
  * Remove unneeded debian/*.dirs files.

 -- Micha Lenk <micha@debian.org>  Mon, 06 Jun 2011 08:51:19 +0200

libktoblzcheck (1.32-1) unstable; urgency=low

  * New upstream release
  * Switch to dpkg-source 3.0 (quilt) format and add the files config.sub and
    config.guess to extend-diff-ignore in debian/source/options. This allows us
    to greatly condense the debian/rules file.
  * Use dh_python2 instead of deprecated python-central (closes: #616866).
    Changes in debian/control:
    - remove python-central from build dependencies
    - add version requirement for build dependency on python-all-dev >= 2.6.6-3~
    - move field XS-Python-Version (deprecated) to X-Python-Version
    - drop all XB-Python-Version fields (now obsolete)
    Changes in debian/rules:
    - call dh sequencer "--with python2" instead of "--with python-central"
  * debian/control: drop obsolete version requirements from build dependencies.
  * debian/control: bump version in build-dep on debhelper due to used overrides
    (thanks to lintian for pointing it out).
  * Package is compliant to Debian Policy 3.9.2.0 (no changes needed).
  * debian/*.docs: drop file TODO which was removed upstream.
  * Add lintian override for soname not matching libktoblzcheck1c2a.
  * debian/control: Remove duplicate section 'libs' from binary package
    libktoblzcheck1c2a (thanks to lintian for the hint).

 -- Micha Lenk <micha@debian.org>  Sun, 05 Jun 2011 01:27:54 +0200

libktoblzcheck (1.31-1) unstable; urgency=low

  * New upstream release

 -- Micha Lenk <micha@debian.org>  Sun, 13 Feb 2011 22:49:04 +0100

libktoblzcheck (1.29-1) unstable; urgency=low

  * New upstream release

 -- Micha Lenk <micha@debian.org>  Sun, 12 Dec 2010 13:18:31 +0100

libktoblzcheck (1.28-1) unstable; urgency=low

  * New upstream release
  * Dropped three obsolete Conflicts/Replaces dependencies:
    - libktoblzcheck1c2a on libktoblzcheck1, last released in Debian Sarge
    - ktoblzcheck on ktoblzcheck-bin (< 1.14-3), last released in Debian Etch
    - ktoblzcheck on libktoblzcheck1c2a (< 1.18), version predates Debian Lenny
  * Package is compliant to Debian Policy 3.9.1

 -- Micha Lenk <micha@debian.org>  Sat, 21 Aug 2010 15:19:01 +0200

libktoblzcheck (1.27-1) unstable; urgency=low

  * New upstream release

 -- Micha Lenk <micha@debian.org>  Mon, 17 May 2010 20:19:28 +0200

libktoblzcheck (1.26-1) unstable; urgency=low

  * New upstream release

 -- Micha Lenk <micha@debian.org>  Wed, 17 Mar 2010 21:22:13 +0100

libktoblzcheck (1.25-1) unstable; urgency=low

  * New upstream release
  * Package is compliant to Debian Policy 3.8.4 (no changes needed)

 -- Micha Lenk <micha@debian.org>  Wed, 03 Mar 2010 19:30:24 +0100

libktoblzcheck (1.24-2) unstable; urgency=low

  * Removed dependency on deprecated package python-ctypes and bump minimal
    Python version to 2.5 (closes: #562473).

 -- Micha Lenk <micha@debian.org>  Sun, 10 Jan 2010 13:57:08 +0100

libktoblzcheck (1.24-1) unstable; urgency=low

  * New upstream release
  * Update maintainer's mail address and drop DM upload privileges.
  * Dropped Georg W. Leonhard from uploaders (invalid mail address and MIA).

 -- Micha Lenk <micha@debian.org>  Tue, 08 Dec 2009 09:45:33 +0100

libktoblzcheck (1.23-1) unstable; urgency=low

  * New upstream release
  * Package is compliant to standards version 3.8.3 (no changes needed).

 -- Micha Lenk <micha@lenk.info>  Wed, 09 Sep 2009 18:44:51 +0200

libktoblzcheck (1.22-1) unstable; urgency=low

  * New upstream release.
  * Drop CDBS in favour of debhelper 7.
  * Drop debian/libktoblzcheck1c2a.shlibs and add an appropriate commandline
    option to dh_makeshlibs instead.
  * Don't patch config.{sub,guess} any more but build-depend on autotools-dev
    and symlink its files instead.
  * Delete debian/docs file and copy its content to each <package>.docs file
    (otherwise the files listed in there won't get installed).
  * Bump shlibsdeps to 1.19 (first one containing the most recent symbols).
  * debian/control:
    + drop transitional package libktoblzcheck-bin (Lenny is released now)
    + split Build-Depends into multiple lines
    + bump standards version to 3.8.2 (no changes needed).

 -- Micha Lenk <micha@lenk.info>  Mon, 22 Jun 2009 10:43:01 +0200

libktoblzcheck (1.21-1) unstable; urgency=low

  * New upstream release
  * debian/rules: configure with --with-wget=/usr/bin/wget to avoid
    build dependency on lynx-curl or lynx
  * debian/control:
    + Drop build dependency on lynx-curl or lynx
    + ktoblzcheck: Switch dependency from lynx-curl or lynx to wget
    + Add several ${misc:Depends} as suggested by lintian. Thanks lintian!
  * debian/watch: Make use of the qa.debian.org redirector for SourceForge

 -- Micha Lenk <micha@lenk.info>  Mon, 16 Feb 2009 09:53:33 +0100

libktoblzcheck (1.18-2) unstable; urgency=low

  * (Build-)depend alternatively on lynx-cur or lynx (not lynx only).
  * Bump standards version (no changes needed).

 -- Micha Lenk <micha@lenk.info>  Sun, 06 Jul 2008 23:51:06 +0200

libktoblzcheck (1.18-1) unstable; urgency=low

  * New upstream release.
  * Refresh patch 00_libtool_debian letting config.guess and config.sub match
    those of autotools-dev 20080123.1.
  * Move online update scripts into package ktoblzcheck. This involves
    following changes in debian/control:
    - Let ktoblzcheck conflict with and replace files from earlier
      libktoblzcheck1c2a,
    - Let libktoblzcheck1c2a suggest package ktoblzcheck,
    - Move dependency on lynx to package ktoblzcheck.
  * Bump version in shlibs file.

 -- Micha Lenk <micha@lenk.info>  Sat, 07 Jun 2008 15:26:03 +0200

libktoblzcheck (1.17-3) unstable; urgency=low

  * debian/rules: Remove empty directory /usr/lib only if it exists (i.e. has
    been left over by dh_pycentral < 0.6 (#452227); closes: #472013).

 -- Micha Lenk <micha@lenk.info>  Mon, 24 Mar 2008 17:15:57 +0100

libktoblzcheck (1.17-2) unstable; urgency=low

  * libktoblzcheck1c2a.postrm: Add globbing for more selective deletion of
    bankdata files on purge (thanks to Thomas Viehmann for this suggestion).

 -- Micha Lenk <micha@lenk.info>  Sun,  2 Mar 2008 18:03:49 +0100

libktoblzcheck (1.17-1) unstable; urgency=low

  * New upstream release
    - includes all needed header files, so it doesn't FTBFS any more with
      GCC 4.3 (closes: #455298).
    - drop patch timezone_bugfix_463205.dpatch (included upstream).
  * Move differences between upstream's and our config.guess and config.sub
    into a clean patch 00_libtool_debian and updating it to the current
    scripts available with libtool 1.5.26-1.
  * Add transitional package libktoblzcheck-bin depending on ktoblzcheck
    allowing a smooth transition from Etch to Lenny (done in prior NMU).
  * Include update scripts online_update.pl (closes: #468228) and
    bundesbank.pl which make libktoblzcheck1c2a depend on perl and lynx.
  * Add build dependency on lynx needed for a working online_update.pl.
  * Move location of data files to /var/lib/ktoblzcheck1/ which is a better
    place for bankdata files being updated by the online_update.pl script.
  * Add postrm script cleaning up the entire directory /var/lib/ktoblzcheck1/
    on purge.
  * Simplify debian/*.install files.
  * Drop static library libktoblzcheck.a and the error-prone linker archive
    libktoblzcheck.la from -dev package.
  * Fix a spelling mistake in the python-ktoblzcheck package description
    (thanks to lintian).
  * debian/rules: Remove empty directory /usr/lib in package
    python-ktoblzcheck after dh_pycentral call (thanks to lintian).

 -- Micha Lenk <micha@lenk.info>  Sat,  1 Mar 2008 20:02:32 +0100

libktoblzcheck (1.16-2.1) unstable; urgency=low

  * NMU to introduce compatibility package in DM-maintained package
  * Add transitional package libktoblzcheck-bin depending on ktoblzcheck
    allowing a smooth transition from Etch to Lenny.

 -- Thomas Viehmann <tv@beamnet.de>  Sun,  2 Mar 2008 01:01:01 +0100

libktoblzcheck (1.16-2) unstable; urgency=low

  [ Micha Lenk ]
  * remove errorneous lintian-override file. It's moreover not needed any
    more.
  * bump standards version from 3.7.2.2 to 3.7.3 (no changes needed)
  * add Vcs-{Svn,Browser} fields
  * re-order package order to ensure that the library package with its shlibs
    file is built first.
  * drop shlibs.local file which isn't needed actually and drop the sync check
    for the shlibs files in debian/rules introduced in previous upload.

  [ Georg W. Leonhardt ]
  * Add support for dpatch system
  * Add patch timezone_bugfix_463205.dpatch (closes: #463205)
    Many thanks to Andreas Pakulat for fix this bug

 -- Micha Lenk <micha@lenk.info>  Mon,  4 Feb 2008 22:58:16 +0100

libktoblzcheck (1.16-1) unstable; urgency=low

  * New upstream release
  * Rework package descriptions (closes: #448548)
  * debian/control: Allow uploads by Debian Maintainers of this package
  * Bump version in shlibs files (closes: #451715)
  * debian/rules: Add a simple check avoiding out of sync shlibs files

 -- Micha Lenk <micha@lenk.info>  Fri, 23 Nov 2007 22:30:05 +0100

libktoblzcheck (1.15-2) unstable; urgency=low

  [ Georg W. Leonhardt ]
  * debian/control: Change package name from python-libktoblzcheck
    to python-ktoblzcheck, because the python package should have the
    same name like the python module

  [ Thomas Viehmann ]
  * Clean up after python package rename

 -- Thomas Viehmann <tv@beamnet.de>  Wed, 17 Oct 2007 23:08:08 +0200

libktoblzcheck (1.15-1) unstable; urgency=low

  [ Georg W. Leonhardt ]
  * Adding myself to uploaders
  * Introduce new package python-libktoblzcheck providing python binding
    for libktoblzcheck
    - add file debian/pycompat
    - debian/control: add python-central and python-all-dev to build-deps
    - debian/control: add XS-Python-Version: all to source section
  * Bump debian/compat. debhelper's changed behaviour doesn't affect the
      package.
  * debian/rules: Add DEB_DH_INSTALL_SOURCEDIR=$(CURDIR)/debian/tmp and
    clean up the *.install files accordingly.

  [ Micha Lenk ]
  * New upstream release
    - drop patch 01_missing_includes_for_gcc_4_3.dpatch (included upstream)
    - drop build depend on dpatch (not needed any more)
  * Rename package libktoblzcheck-bin to ktoblzcheck. This way it matches the
    binary name and is easier to find.
  * debian/control: Switch from deprecated ${Source-Version} to
    ${binary:Version}, making the package binNMU safe.

 -- Micha Lenk <micha@lenk.info>  Mon, 17 Sep 2007 19:47:20 +0200

libktoblzcheck (1.14-2) unstable; urgency=low

  * Build depend on dpatch
  * Apply Martin Michlmayr's patch to include header files needed by GCC 4.3
    (closes: #417335)

 -- Micha Lenk <micha@lenk.info>  Mon,  9 Jul 2007 02:54:20 +0200

libktoblzcheck (1.14-1) unstable; urgency=low

  * New upstream release
  * debian/copyright: FSF address update
  * bump standards version (no changes needed)

 -- Micha Lenk <micha@lenk.info>  Mon,  9 Jul 2007 02:04:01 +0200

libktoblzcheck (1.13-1) unstable; urgency=low

  * New upstream release

 -- Micha Lenk <micha@lenk.info>  Sun, 25 Feb 2007 12:04:50 +0100

libktoblzcheck (1.10-1) unstable; urgency=low

  * New upstream release
  * Adding myself to uploaders

 -- Micha Lenk <micha@lenk.info>  Tue, 28 Mar 2006 19:15:46 +0200

libktoblzcheck (1.9-1) unstable; urgency=low

  * New upstream release

 -- Thomas Viehmann <tv@beamnet.de>  Sat, 14 Jan 2006 15:47:15 +0100

libktoblzcheck (1.8-1) unstable; urgency=low

  * New upstream release
  * Renaming library for mt_alloc reference problems. Closes: #339209.

 -- Thomas Viehmann <tv@beamnet.de>  Thu, 17 Nov 2005 07:12:36 +0100

libktoblzcheck (1.7-1) unstable; urgency=low

  * New upstream release
    Updates libtool. Closes: #320505.
  * Added lintian override for impossible linebreak due to a long URL
    in man page.

 -- Thomas Viehmann <tv@beamnet.de>  Sat, 20 Aug 2005 15:12:54 +0200

libktoblzcheck (1.6-3) unstable; urgency=low

  * Everything that can go wrong will go wrong. Adjust shlibs file
    for C++ transition's package rename.

 -- Thomas Viehmann <tv@beamnet.de>  Tue, 26 Jul 2005 07:56:20 +0200

libktoblzcheck (1.6-2) unstable; urgency=low

  * Really do the transition properly. Apologies to Joerg Jaspert
    and thanks for stopping the broken -1.

 -- Thomas Viehmann <tv@beamnet.de>  Thu, 21 Jul 2005 09:49:37 +0200

libktoblzcheck (1.6-1) unstable; urgency=low

  * New upstream release
  * g++ transition (libktoblzcheck1 renamed to libktoblzcheck1c2)
  * Bump Standards to 3.6.2 (no changes).

 -- Thomas Viehmann <tv@beamnet.de>  Wed, 20 Jul 2005 09:07:27 +0200

libktoblzcheck (1.2-1) unstable; urgency=low

  * New upstream release
  * Gratefully added co-maintainer Henning Glawe
  * Switch to CDBS
  * Add watch file

 -- Thomas Viehmann <tv@beamnet.de>  Fri, 26 Nov 2004 19:34:46 +0100

libktoblzcheck (0.6-1) unstable; urgency=low

  * New upstream release
  * Include copyright in copyright.

 -- Thomas Viehmann <tv@beamnet.de>  Fri,  2 Jan 2004 13:46:09 +0100

libktoblzcheck (0.5-1) unstable; urgency=low

  * New upstream release

 -- Thomas Viehmann <tv@beamnet.de>  Tue, 25 Nov 2003 21:41:12 +0100

libktoblzcheck (0.4-1) unstable; urgency=low

  * Initial Release.
  * Upload to debian. Closes: #205117.

 -- Thomas Viehmann <tv@beamnet.de>  Sat, 30 Aug 2003 22:06:31 +0200
