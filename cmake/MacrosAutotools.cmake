# CMake package for autotools support
#
# Copyright (c) 2011-2019 Ralf Habacker, <ralf.habacker@freenet.de>
#
# The Regents of the University of California. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The name of the author may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

#
# @Author Ralf Habacker <ralf.habacker@freenet.de>
# @version 1.1
#

#
# load autotools configure file into an internal list named _configure_ac
#
macro(autoinit config)
    set(_configure_ac_name ${config})
    file(READ ${config} _configure_ac_raw)
    # Convert file contents into a CMake list (where each element in the list
    # is one line of the file)
    string(REGEX REPLACE ";" "\\\\;" _configure_ac "${_configure_ac_raw}")
    string(REGEX REPLACE "\n" ";" _configure_ac "${_configure_ac}")
endmacro()

#
# extracts version information from autoconf config file
# and set related cmake variables
#
# returns
#   ${prefix}_VERSION
#   ${prefix}_VERSION_STRING
#   ${prefix}_VERSION_MAJOR
#   ${prefix}_VERSION_MINOR
#   ${prefix}_VERSION_MICRO
#
macro(autoversion prefix)
    string(TOUPPER ${prefix} prefix_upper)
    if(_configure_ac_raw MATCHES ".*${prefix}_major_version], .([0-9]+).*")
        string(REGEX REPLACE ".*${prefix}_major_version], .([0-9]+).*" "\\1" ${prefix_upper}_VERSION_MAJOR ${_configure_ac_raw})
        string(REGEX REPLACE ".*${prefix}_minor_version], .([0-9]+).*" "\\1" ${prefix_upper}_VERSION_MINOR ${_configure_ac_raw})
        string(REGEX REPLACE ".*${prefix}_micro_version], .([0-9]+).*" "\\1" ${prefix_upper}_VERSION_MICRO ${_configure_ac_raw})
        set(${prefix_upper}_VERSION ${${prefix_upper}_VERSION_MAJOR}.${${prefix_upper}_VERSION_MINOR}.${${prefix_upper}_VERSION_MICRO})
    else()
        string(REGEX REPLACE ".*VERSION_MAJOR=[\"]*([0-9]+)[\"]*.*" "\\1" ${prefix_upper}_VERSION_MAJOR ${_configure_ac_raw})
        string(REGEX REPLACE ".*VERSION_MINOR=[\"]*([0-9]+)[\"]*.*" "\\1" ${prefix_upper}_VERSION_MINOR ${_configure_ac_raw})
        set(${prefix_upper}_VERSION ${${prefix_upper}_VERSION_MAJOR}.${${prefix_upper}_VERSION_MINOR})
    endif()
    set(${prefix_upper}_VERSION_STRING "${${prefix_upper}_VERSION}")
endmacro()

#
# extracts library version information from autoconf config file
# and set related cmake variables
#
#   ${prefix}_LIBRARY_AGE
#   ${prefix}_LIBRARY_REVISION
#   ${prefix}_LIBRARY_CURRENT
#
macro(autolibraryversion prefix)
    string(TOUPPER ${prefix} prefix_upper)
    string (REGEX REPLACE ".*_AGE=([0-9]+).*" "\\1" ${prefix_upper}_LIBRARY_AGE ${_configure_ac_raw})
    string (REGEX REPLACE ".*_CURRENT=([0-9]+).*" "\\1" ${prefix_upper}_LIBRARY_CURRENT ${_configure_ac_raw})
    string (REGEX REPLACE ".*_REVISION=([0-9]+).*" "\\1" ${prefix_upper}_LIBRARY_REVISION ${_configure_ac_raw})
endmacro()

#
# Defines package related variables (PACKAGE_..., PACKAGE and VERSION)
# as done by autotools.
#
macro(autopackage name version url support_url)
    # Define to the full name of this package.
    set(PACKAGE_NAME ${name})

    # Define to the version of this package.
    set(PACKAGE_VERSION ${version})

    # Define to the home page for this package.
    set(PACKAGE_URL ${url})

    # Define to the address where bug reports for this package should be sent.
    set(PACKAGE_BUGREPORT ${support_url})

    # Define to the full name and version of this package.
    set(PACKAGE_STRING "${PACKAGE_NAME} ${PACKAGE_VERSION}")

    # Define to the one symbol short name of this package.
    set(PACKAGE_TARNAME ${PACKAGE_NAME})

    set(PACKAGE ${PACKAGE_NAME})
    set(VERSION ${PACKAGE_VERSION})
endmacro()

#
# Defines a cmake variable named PACKAGE_CONFIG_H_TEMPLATE
# which could be placed in config.h templates to have those variables
# defined at code level like shown below:
#
# config.h.template
#   ...
#   @AUTOPACKAGE_CONFIG_H_TEMPLATE@
#   ...
#
macro(autopackage_config_h_template)
    string(CONFIGURE "/* generated by cmake macro autopackage */\n
/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT \"@PACKAGE_BUGREPORT@\"

/* Define to the full name of this package. */
#define PACKAGE_NAME \"@PACKAGE_NAME@\"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING \"@PACKAGE_STRING@\"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME \"@PACKAGE_TARNAME@\"

/* Define to the home page for this package. */
#define PACKAGE_URL \"@PACKAGE_URL@\"

/* Define to the version of this package. */
#define PACKAGE_VERSION \"@PACKAGE_VERSION@\"

/* defined by autotools package */
#define PACKAGE \"@PACKAGE@\"
#define VERSION \"@VERSION@\"
" AUTOPACKAGE_CONFIG_H_TEMPLATE)
endmacro()

#
# define a cmake variable from autotools AC_DEFINE statement
#
macro(autodefine name)
    foreach(line ${_configure_ac})
        if(line MATCHES ".*AC_DEFINE(.*${name}.*).*")
            string (REGEX REPLACE ".*AC_DEFINE(.*).*" "\\1" value ${line})
            string (REGEX REPLACE "[^[]*\\[[^]]*\\], *\\[([^]]*)\\],.*" "\\1" value2 ${value})
            string (REPLACE "[" "" value3 ${value2})
            string (REPLACE "]" "" value4 ${value3})
            set(${name} ${value4})
        endif()
    endforeach()
endmacro()

#
# helper macro to check consistence between autotools config.h template,
# cmake file containing configure checks and cmake specific config.h template
#
macro(autoheaderchecks config_h_in configure_checks_file config_h_cmake)
    file(READ ${configure_checks_file} configure_checks_file_raw)
    file(READ ${config_h_in} _config_h_in_raw)
    file(READ ${config_h_cmake} _config_h_cmake_raw)
    STRING(REGEX REPLACE ";" "\\\\;" _config_h_in "${_config_h_in_raw}")
    STRING(REGEX REPLACE "\n" ";" _config_h_in "${_config_h_in}")
    foreach(line ${_config_h_in})
        #message(STATUS ${line})
        if(line MATCHES ".*HAVE_.*_H.*")
            string (REGEX REPLACE ".*HAVE_(.*)_H.*" "\\1" key ${line})
            set(full_key "HAVE_${key}_H")
            if(key MATCHES ".*_.*")
                string(REGEX MATCH "^[A-Z0-9]+" dir ${key})
                string(REGEX MATCH "[A-Z0-9]+$" file ${key})
                string(TOLOWER ${dir} dirname)
                string(TOLOWER ${file} filename)
                set(check "check_include_file(${dirname}/${filename}.h     ${full_key})")
                set(config_define "#cmakedefine ${full_key}")
            else()
                set(file ${key})
                string(TOLOWER ${file} filename)
                set(check "check_include_file(${filename}.h     ${full_key})")
                set(config_define "#cmakedefine ${full_key}")
            endif()
            if(NOT configure_checks_file_raw MATCHES ".*${full_key}.*")
                message("${check}")
            endif()
            if(NOT _config_h_cmake_raw MATCHES "${full_key}")
                message("${config_define}")
            endif()
        endif()
    endforeach()
endmacro(autoheaderchecks)

#
# parses config.h template and create cmake equivalent
#
macro(autoconfig template outfile)
    file(READ ${template} contents)
    STRING(REGEX REPLACE ";" "\\\\;" contents "${contents}")
    STRING(REGEX REPLACE "\n" ";" contents "${contents}")
    set(out)
    foreach(line ${contents})
        if(line MATCHES ".*#undef ENABLE.*")
            string(REGEX REPLACE "^#undef (.*)$" "#cmakedefine \\1" a ${line})
            set(out "${out}\n${a}")
        elseif(line MATCHES ".*#undef HAVE.*")
            string(REGEX REPLACE "^#undef (.*)$" "#cmakedefine \\1" a ${line})
            set(out "${out}\n${a}")
        elseif(line MATCHES ".*#undef OS.*")
            string(REGEX REPLACE "^#undef (.*)$" "#cmakedefine \\1 @\\1@" a ${line})
            set(out "${out}\n${a}")
        elseif(line MATCHES ".*#undef.*")
            string(REGEX REPLACE "^#undef (.*)$" "#cmakedefine \\1 \"@\\1@\"" a ${line})
            set(out "${out}\n${a}")
        else()
            set(out "${out}\n${line}")
        endif()
    endforeach()
    #set(contents "${contents}\n@AUTOPACKAGE_CONFIG_H_TEMPLATE@")
    file(WRITE ${outfile} "${out}")
endmacro()
