cmake_minimum_required(VERSION 3.0)

project(ktoblzcheck VERSION 1.53.0)

# define path for local cmake modules
list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)

########### options ###############
option(ENABLE_BANKDATA_DOWNLOAD "Enable download of raw bank data file" ON)
option(INSTALL_RAW_BANKDATA_FILE "Install raw bank data file" OFF)

########### global settings ###############
set(BANK_WEBSITE_HOST https://www.bundesbank.de)
set(BANK_WEBSITE_PATH /de/aufgaben/unbarer-zahlungsverkehr/serviceangebot/bankleitzahlen/download---bankleitzahlen-602592)
# format description
#https://www.bundesbank.de/resource/blob/602848/50cba8afda2b0b1442016101cfd7e084/mL/merkblatt-bankleitzahlendatei-data.pdf

option(INSTALL_SEPA_BANKDATA_FILE "Install sepa provider data file in csv format" OFF)
option(BUILD_STATIC "build static library and executables" OFF)
if(BUILD_STATIC)
    set(LIBS -static -static-libgcc -static-libstdc++)
    set(CMAKE_BUILD_TYPE Release)
endif()

# provide gnu compatible install dirs
include(GNUInstallDirs)

# definitions for running cross compile applications
include(MacrosWine)

# common macros
include(Macros)

# set PACKAGE_... variables
include(MacrosAutotools)
autopackage(${PROJECT_NAME} ${PROJECT_VERSION} "${PROJECT_HOMEPAGE_URL}" "https://sourceforge.net/projects/ktoblzcheck/support")

# definitions in source file
set(KTOBLZCHECK_VERSION_MAJOR ${PROJECT_VERSION_MAJOR})
set(KTOBLZCHECK_VERSION_MINOR ${PROJECT_VERSION_MINOR})
set(KTOBLZCHECK_VERSION_PATCH ${PROJECT_VERSION_PATCH})

# shared library version numbers
set(LIBKTOBLZCHECK_CURRENT 6)
set(LIBKTOBLZCHECK_AGE 5)
set(LIBKTOBLZCHECK_REVISION 26)
math(EXPR LIBKTOBLZCHECK_EFFECTIVE "${LIBKTOBLZCHECK_CURRENT} - ${LIBKTOBLZCHECK_AGE}")

if(WIN32)
    set(OS_WIN32 1)
endif()

# target build dir defaults
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")
if(WIN32)
   set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")
   set(CUSTOM_INSTALL_LIBDIR ${CMAKE_INSTALL_BINDIR})
else()
   set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")
   set(CUSTOM_INSTALL_LIBDIR ${CMAKE_INSTALL_LIBDIR})
endif()

# target install defaults
set(INSTALL_TARGETS_DEFAULT_ARGS
    EXPORT KtoBlzCheckTargets
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
)

# for embedding into installed header files
set(VERSION_MAJOR ${KTOBLZCHECK_VERSION_MAJOR})
set(VERSION_MINOR ${KTOBLZCHECK_VERSION_MINOR})

########### external packages ###############
find_package(Doxygen "1.8.3")

set(CMAKE_THREAD_PREFER_PTHREAD 1)
find_package(Threads REQUIRED)
if(CMAKE_USE_PTHREADS_INIT)
    set(HAVE_LIBPTHREAD 1)
endif()

find_package (Python3 COMPONENTS Interpreter Development)
if(Python3_FOUND)
    set(Python_EXECUTABLE ${Python3_EXECUTABLE})
    string(REGEX REPLACE "^([0-9]+.[0-9]+).*" "\\1" Python_VERSION_MAJOR_MINOR ${Python3_VERSION})
    set(Python_SITEARCH "${CMAKE_INSTALL_LIBDIR}/python${Python_VERSION_MAJOR_MINOR}/site-packages")
    set(Python_Interpreter_FOUND TRUE)
    set(Python_Development_FOUND TRUE)
else()
    find_package(Python2 COMPONENTS Interpreter Development)
    # for cmake < 3.12
    if(Python2_Interpreter_FOUND)
        set(Python_EXECUTABLE ${Python2_EXECUTABLE})
        set(Python_Interpreter_FOUND TRUE)
    else()
        find_package(PythonInterp 2.7 REQUIRED)
        set(Python_Interpreter_FOUND TRUE)
        set(Python_EXECUTABLE ${PYTHON_EXECUTABLE})
    endif()
    if(Python2_Development_FOUND)
        string(REGEX REPLACE "^([0-9]+.[0-9]+).*" "\\1" Python_VERSION_MAJOR_MINOR ${Python2_VERSION})
        set(Python_SITEARCH "${CMAKE_INSTALL_LIBDIR}/python${Python_VERSION_MAJOR_MINOR}/site-packages")
        set(Python_Development_FOUND TRUE)
    else()
        find_package(PythonLibs 2.6 REQUIRED)
        set(Python_Development_FOUND TRUE)
        set(Python2_VERSION ${PYTHONLIBS_VERSION_STRING})
        string(REGEX REPLACE "^([0-9]+.[0-9]+).*" "\\1" Python_VERSION_MAJOR_MINOR ${Python2_VERSION})
        set(Python_SITEARCH "${CMAKE_INSTALL_LIBDIR}/python${Python_VERSION_MAJOR_MINOR}/site-packages")
    endif()
endif()
########### configure checks ###############
include(CheckIncludeFile)
check_include_file(dirent.h     HAVE_DIRENT_H)
check_include_file(dlfcn.h      HAVE_DLFCN_H)
check_include_file(inttypes.h   HAVE_INTTYPES_H)
check_include_file(memory.h     HAVE_MEMORY_H)
check_include_file(stdint.h     HAVE_STDINT_H)
check_include_file(stdlib.h     HAVE_STDLIB_H)
check_include_file(stdio.h      HAVE_STDIO_H)
check_include_file(string.h     HAVE_STRING_H)
check_include_file(strings.h    HAVE_STRINGS_H)
check_include_file(syslog.h     HAVE_SYSLOG_H)
check_include_file(sys/stat.h   HAVE_SYS_STAT_H)
check_include_file(sys/types.h  HAVE_SYS_TYPES_H)
check_include_file(windows.h    HAVE_WINDOWS_H)

include(CheckTypeSize)
check_type_size("long long" LONG_LONG)


# bankdata related variables
set(BANKDATA_SRCPATH ${CMAKE_SOURCE_DIR}/src/bankdata)
if(ENABLE_BANKDATA_DOWNLOAD)
    string(TIMESTAMP _BANKDATA_FILE "bankdata_%Y%m%d.txt")
    set(BANKDATA_FILE ${_BANKDATA_FILE})
    set(BANKDATA_FILEPATH "${BANKDATA_SRCPATH}/${BANKDATA_FILE}")
    message(STATUS "Using bank data file ${BANKDATA_FILEPATH}")

    string(TIMESTAMP _BANKDATA_RAW_FILE "blz_%Y%m%d.txt")
    set(BANKDATA_RAW_FILE ${_BANKDATA_RAW_FILE})
    set(BANKDATA_RAW_FILEPATH "${BANKDATA_SRCPATH}/${BANKDATA_RAW_FILE}")
    message(STATUS "Using raw bank data file ${BANKDATA_RAW_FILEPATH}")
else()
    file(GLOB BANKDATA_FILEPATH  "${BANKDATA_SRCPATH}/bankdata_*.txt")
    get_filename_component(BANKDATA_FILE ${BANKDATA_FILEPATH} NAME)
    message(STATUS "Using present bank data file ${BANKDATA_FILEPATH}")

    file(GLOB BANKDATA_RAW_FILEPATH  "${BANKDATA_SRCPATH}/blz_*.txt")
    get_filename_component(BANKDATA_RAW_FILE ${BANKDATA_RAW_FILEPATH} NAME)
    message(STATUS "Using present raw bank data file ${BANKDATA_RAW_FILEPATH}")
endif()
set(BANKDATA_PATH ${CMAKE_INSTALL_FULL_DATADIR}/ktoblzcheck)
add_definitions(-DBANKDATA_PATH="${BANKDATA_PATH}")

########### generate pkgconfig file ###############
set(prefix "\${pcfiledir}/../..")
set(exec_prefix "\${prefix}")
set(bindir "\${prefix}/${CMAKE_INSTALL_BINDIR}")
set(libdir "\${prefix}/${CMAKE_INSTALL_LIBDIR}")
set(includedir "\${prefix}/${CMAKE_INSTALL_INCLUDEDIR}")
set(datadir "\${prefix}/${CMAKE_INSTALL_DATADIR}")
if (Python_SITEARCH)
    set(pythondir "\${prefix}${Python_SITEARCH}")
endif()

configure_file(${PACKAGE}.pc.in ${CMAKE_CURRENT_BINARY_DIR}/${PACKAGE}.pc @ONLY)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${PACKAGE}.pc DESTINATION ${CMAKE_INSTALL_LIBDIR}/pkgconfig)

########### generate cmake find_package file ###############
include(CMakePackageConfigHelpers)

set(INSTALL_CMAKE_DIR ${CMAKE_INSTALL_LIBDIR}/cmake/KtoBlzCheck)
configure_package_config_file(KtoBlzCheckConfig.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/KtoBlzCheckConfig.cmake
    INSTALL_DESTINATION ${INSTALL_CMAKE_DIR}
)
write_basic_package_version_file(
    ${CMAKE_CURRENT_BINARY_DIR}/KtoBlzCheckConfigVersion.cmake
    VERSION ${PROJECT_VERSION}
    COMPATIBILITY AnyNewerVersion
)

install(EXPORT KtoBlzCheckTargets
    FILE KtoBlzCheckTargets.cmake
    DESTINATION ${INSTALL_CMAKE_DIR}
)

# used by install autotests
export(EXPORT KtoBlzCheckTargets FILE KtoBlzCheckTargets.cmake)

install(FILES
    ${CMAKE_CURRENT_BINARY_DIR}/KtoBlzCheckConfig.cmake
    ${CMAKE_CURRENT_BINARY_DIR}/KtoBlzCheckConfigVersion.cmake
    DESTINATION ${INSTALL_CMAKE_DIR}
)

########### generate config.h ###############
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/config.h.cmake ${CMAKE_CURRENT_BINARY_DIR}/config.h)
add_definitions(
    -DHAVE_CONFIG_H
)

########### generate spec file ###############
configure_file(ktoblzcheck.spec.in ${CMAKE_BINARY_DIR}/ktoblzcheck.spec)

########### generate tarball ###############
set(tarname ${PACKAGE}-${PROJECT_VERSION})
set(temppath ${CMAKE_BINARY_DIR}/tmp/${tarname})
set(tarball ${CMAKE_BINARY_DIR}/${tarname}.tar.gz)
add_custom_command(OUTPUT ${temppath}
    COMMAND ${CMAKE_COMMAND} -E make_directory ${temppath}
)

add_custom_target(dist
    COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}  ${temppath}
    COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_BINARY_DIR}/config.h.cmake ${temppath}
    COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_BINARY_DIR}/ktoblzcheck.spec ${temppath}
    COMMAND ${CMAKE_COMMAND} -E tar -czf ${tarball} ${tarname}
    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/tmp
    COMMENT "generating ${tarball}"
    DEPENDS ${temppath} ${CMAKE_BINARY_DIR}/ktoblzcheck.spec ${CMAKE_BINARY_DIR}/config.h.cmake
)

add_custom_target(rpm
    COMMAND rpmbuild -ta ${tarball}
    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
    COMMENT "generating rpm file"
    DEPENDS dist
)

add_custom_target(srpm
    COMMAND rpmbuild -ts ${tarball}
    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
    COMMENT "generating srpm file"
    DEPENDS dist
)

#
# add package_source target
#

# find files not in git repo
exec_program(git ${CMAKE_SOURCE_DIR}
    ARGS ls-files --others --directory
    OUTPUT_VARIABLE IGNORE_FILES
)
string(REGEX REPLACE "\n" ";" IGNORE_FILES "${IGNORE_FILES}")

# did not find a way to disable binary generator for now
set(CPACK_GENERATOR "XX")
set(CPACK_STRIP_FILES TRUE)
set(CPACK_SOURCE_GENERATOR "TGZ")
set(CPACK_SOURCE_IGNORE_FILES ".git;${IGNORE_FILES}")
set(CPACK_SOURCE_PACKAGE_FILE_NAME "${PROJECT_NAME}-${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}")
include(CPack)

########### generate installer ###############
if(WIN32)
    find_file(
        ISCC_EXECUTABLE ISCC.exe
        PATH
            ${ISCC_DIR}
            "$ENV{ProgramFiles}/Inno\ Setup\ 5/"
            "$ENV{ProgramFiles}/Inno\ Setup\ 6/"
        NO_DEFAULT_PATH
    )
    if (ISCC_EXECUTABLE)
        message(STATUS "Inno Setup compiler found at ${ISCC_EXECUTABLE}")
        configure_file(${CMAKE_CURRENT_SOURCE_DIR}/ktoblzcheck.iss.in ${CMAKE_CURRENT_BINARY_DIR}/ktoblzcheck.iss)
        set(FILES "README;COPYING;README.WIN32;AUTHORS;ChangeLog")
        foreach(f ${FILES})
            configure_file(${CMAKE_CURRENT_SOURCE_DIR}/${f} ${CMAKE_CURRENT_BINARY_DIR}/${f} COPYONLY)
        endforeach()
        add_custom_target(setup
            COMMAND ${CMAKE_MAKE_PROGRAM} install DESTDIR=${CMAKE_CURRENT_BINARY_DIR}/win32-tmp
            COMMAND ${CMAKE_COMMAND} -E copy_directory
                ${CMAKE_CURRENT_BINARY_DIR}/win32-tmp${CMAKE_INSTALL_PREFIX}
                ${CMAKE_CURRENT_BINARY_DIR}/win32-tmp
            COMMAND wine ${ISCC_EXECUTABLE} z:${CMAKE_CURRENT_BINARY_DIR}/ktoblzcheck.iss
            WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        )
    endif()
endif()

# enable ctest
enable_testing()

add_subdirectory(autotests)
add_subdirectory(src)
add_subdirectory(doc)


add_custom_target(uncrustify
    COMMAND ${CMAKE_SOURCE_DIR}/uncrustify.sh
    COMMENT "uncrustify sources"
)

include(FeatureSummary)
feature_summary(WHAT ALL)
