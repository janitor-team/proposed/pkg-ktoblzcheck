
/* config.h.in.  Generated from configure.ac by autoheader.  */
/* Include pthread support for binary relocation? */
#cmakedefine BR_PTHREAD "@BR_PTHREAD@"
/* Define if you want debugging code enabled. */
#cmakedefine DEBUG "@DEBUG@"
/* Use binary relocation? */
#cmakedefine ENABLE_BINRELOC
/* Define to 1 if you have the <dirent.h> header file. */
#cmakedefine HAVE_DIRENT_H
/* Define to 1 if you have the <dlfcn.h> header file. */
#cmakedefine HAVE_DLFCN_H
/* Define to 1 if you have the <inttypes.h> header file. */
#cmakedefine HAVE_INTTYPES_H
/* Define to 1 if you have the `pthread' library (-lpthread). */
#cmakedefine HAVE_LIBPTHREAD
/* Defined if the compiler supports long long int */
#cmakedefine HAVE_LONG_LONG
/* Define to 1 if you have the <memory.h> header file. */
#cmakedefine HAVE_MEMORY_H
/* Define to 1 if you have the <stdint.h> header file. */
#cmakedefine HAVE_STDINT_H
/* Define to 1 if you have the <stdlib.h> header file. */
#cmakedefine HAVE_STDLIB_H
/* Define to 1 if you have the <strings.h> header file. */
#cmakedefine HAVE_STRINGS_H
/* Define to 1 if you have the <string.h> header file. */
#cmakedefine HAVE_STRING_H
/* Define to 1 if you have the <sys/stat.h> header file. */
#cmakedefine HAVE_SYS_STAT_H
/* Define to 1 if you have the <sys/types.h> header file. */
#cmakedefine HAVE_SYS_TYPES_H
/* Define to 1 if you have the <unistd.h> header file. */
#cmakedefine HAVE_UNISTD_H
/* Define to 1 if you have the <windows.h> header file. */
#cmakedefine HAVE_WINDOWS_H
/* Define to the sub-directory in which libtool stores uninstalled libraries.
   */
#cmakedefine LT_OBJDIR "@LT_OBJDIR@"
/* Defined if Windows is used */
#cmakedefine OS_WIN32 @OS_WIN32@
/* Name of package */
#cmakedefine PACKAGE "@PACKAGE@"
/* Define to the address where bug reports for this package should be sent. */
#cmakedefine PACKAGE_BUGREPORT "@PACKAGE_BUGREPORT@"
/* Define to the full name of this package. */
#cmakedefine PACKAGE_NAME "@PACKAGE_NAME@"
/* Define to the full name and version of this package. */
#cmakedefine PACKAGE_STRING "@PACKAGE_STRING@"
/* Define to the one symbol short name of this package. */
#cmakedefine PACKAGE_TARNAME "@PACKAGE_TARNAME@"
/* Define to the home page for this package. */
#cmakedefine PACKAGE_URL "@PACKAGE_URL@"
/* Define to the version of this package. */
#cmakedefine PACKAGE_VERSION "@PACKAGE_VERSION@"
/* Define to 1 if you have the ANSI C header files. */
#cmakedefine STDC_HEADERS "@STDC_HEADERS@"
/* Version number of package */
#cmakedefine VERSION "@VERSION@"