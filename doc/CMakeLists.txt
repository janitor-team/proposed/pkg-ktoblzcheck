
# build doc always
add_custom_target(doc ALL)

if(DOXYGEN_EXECUTABLE)
    set(ENABLE_DOXYGEN_DOCS 1)
endif()

if(ENABLE_DOXYGEN_DOCS)
    set (top_srcdir ${CMAKE_SOURCE_DIR})
    configure_file(../Doxyfile.in ${CMAKE_BINARY_DIR}/Doxyfile )
    add_custom_command(
        OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/api/html/index.html
        COMMAND ${DOXYGEN_EXECUTABLE} ${CMAKE_BINARY_DIR}/Doxyfile
        WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
    )
    add_custom_target(apidoc
        DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/api/html/index.html
    )
    add_dependencies(doc apidoc)
    install(DIRECTORY ${CMAKE_BINARY_DIR}/apidoc/html/ DESTINATION ${CMAKE_INSTALL_DATADIR}/doc/ktoblzcheck/api)
endif()

install(FILES ibanchk.1 ktoblzcheck.1 DESTINATION ${CMAKE_INSTALL_DATADIR}/man/man1)
