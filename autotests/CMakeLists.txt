add_test(install ${CMAKE_CTEST_COMMAND}
    --build-and-test
    "${CMAKE_CURRENT_SOURCE_DIR}/install"
    "${CMAKE_CURRENT_BINARY_DIR}/install"
    --build-generator ${CMAKE_GENERATOR}
    --build-makeprogram ${CMAKE_MAKE_PROGRAM}
    --build-target install
    --build-options
        "-DCMAKE_MODULE_PATH=${CMAKE_MODULE_PATH}"
        "-DCMAKE_INSTALL_PREFIX:PATH=${CMAKE_CURRENT_BINARY_DIR}/install/destdir"
        "-DKtoBlzCheck_DIR=${CMAKE_BINARY_DIR}"
        "-DCHECK_VERSION=${PROJECT_VERSION}"
)
