
include_directories(
    ${CMAKE_SOURCE_DIR}/src/include
    ${CMAKE_BINARY_DIR}
    ${CMAKE_BINARY_DIR}/src/include
)

########### next target ###############

set(ktoblzcheck_LIB_SRCS
    ktoblzcheck.cc
    methods.cc
    algorithms.cc
    accnum.cc
    iban.cc
)
if(BUILD_STATIC)
    set(MODE STATIC)
else()
    set(MODE SHARED)
endif()

add_library(ktoblzcheck ${MODE} ${ktoblzcheck_LIB_SRCS})
target_link_libraries(ktoblzcheck stdc++ ${LIBS})
if(WIN32)
    set_target_properties(ktoblzcheck PROPERTIES SUFFIX "-${LIBKTOBLZCHECK_EFFECTIVE}${CMAKE_SHARED_LIBRARY_SUFFIX}")
else()
    set_target_properties(ktoblzcheck PROPERTIES VERSION ${LIBKTOBLZCHECK_EFFECTIVE}.${LIBKTOBLZCHECK_AGE}.${LIBKTOBLZCHECK_REVISION} SOVERSION ${LIBKTOBLZCHECK_EFFECTIVE})
endif()

add_dependencies(ktoblzcheck bankdata_de)
add_dependencies(ktoblzcheck fetch_sepa)

install(TARGETS ktoblzcheck ${INSTALL_TARGETS_DEFAULT_ARGS})

########### next target ###############

set(iban_check_SRCS iban_check.cc)
add_executable(iban_check ${iban_check_SRCS})
target_link_libraries(iban_check ktoblzcheck)
add_test(
    NAME iban_check
    COMMAND ${TEST_WRAPPER} $<TARGET_FILE:iban_check>
)
set(env "BANKDATA_SRCPATH=${BANKDATA_SRCPATH}")
set_tests_properties(iban_check PROPERTIES ENVIRONMENT "${env}")

########### next target ###############

set(date_test_SRCS ktoblzcheck.cc)
add_executable(date_test ${date_test_SRCS})
target_link_libraries(date_test)
target_compile_definitions(date_test PUBLIC ONLY_EXTRACT_DATE_TEST=1)
add_test(
    NAME date_test
    COMMAND ${TEST_WRAPPER} $<TARGET_FILE:date_test>
)
